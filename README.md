# authenticate-angular

## Usage
Configurazione:
 
```
angular
	.module("app", ['authentica'])
	.config(function ($httpProvider) { 
		$httpProvider.interceptors.push('AuthInterceptor'); 
	});
```

Libreria:

```
AuthService.login({ username: 'dzambuto', password: 'miao' })
AuthService.logout()
AuthService.forgotPassword({ email: 'daniele.zambuto@mail.com' })
AuthService.changePassword({ username: 'dzambuto', password: 'miao' })
AuthService.signUp({ username: 'dzambuto', password: 'miao', fullname: 'Daniele Zambuto', email: 'daniele.zambuto@mail.com' })
```

Eventi:
```
loginSuccess: 'auth:login:success',
loginFailed: 'auth:login:failed',
logoutSuccess: 'auth:logout:success',
sessionTimeout: 'auth:session:timeout',
notAuthenticated: 'auth:not:authenticated'
```