export PATH := ./node_modules/.bin/:./build/node_modules/.bin:$(PATH)

dist/authentica-angular.js: $(wildcard lib/*.js lib/**/*.js node_modules/*/package.json)
	browserify lib/index.js --debug --outfile $@
	
dist/authentica-angular.annotate.js: dist/authentica-angular.js
	ng-annotate -a $< -o $@

dist/authentica-angular.min.js: dist/authentica-angular.annotate.js
	uglifyjs $< --compress warnings=false --mangle --output $@
	
scripts: dist/authentica-angular.js #build/www/js/authentica-angular.annotate.js build/www/js/authentica-angular.min.js

build: clean scripts

clean:
	rm -rf dist/*.js
		
.PHONY: scripts build clean