(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

angular
  .module('authentica')
  .provider('AuthInterceptor', AuthInterceptor);

function AuthInterceptor() {
  var config = this;

  this.buffered = true;
  this.urlParam = null;
  this.authHeader = 'Authorization';
  this.authPrefix = 'JWT ';

  this.$get = ["$rootScope", "$q", "$log", "Session", "HttpBuffer", "AuthEvents", function ($rootScope, $q, $log, Session, HttpBuffer, AuthEvents) {
    return {
      request: requestHandler,
      responseError: responseError
    };

    function requestHandler (request) {
      if (request.skipAuthorization || request.url == '/login') {
        return request;
      }

      /*if (config.urlParam) {
        request.params = request.params || {};
        if (request.params[config.urlParam]) {
          return request;
        }
      } else {
        request.headers = request.headers || {};
        if (request.headers[config.authHeader]) {
          return request;
        }
      }*/

      var token = Session.token;

      if (token) {
        if (config.urlParam) {
          request.params[config.urlParam] = token;
        } else {
          request.headers[config.authHeader] = config.authPrefix + token;
        }
      }

      return request;
    }

    function responseError (response) {
      switch (response.status) {
        case 401: // user not logged in
          var deferred;

          if(response.config.url.indexOf('/login') != -1)
            break;

          $log.debug('AuthInterceptor - Utente non autenticato (HTTP 401)');
          deferred = $q.defer()

          if (config.buffered)
            HttpBuffer.append(response.config, deferred);

          Session.destroy();

          $rootScope.$broadcast(AuthEvents.notAuthenticated, response);
          return deferred.promise;
          break;

        case 403:
          $log.debug('AuthInterceptor - Utente non autorizzato (HTTP 403)');
          $rootScope.$broadcast(AuthEvents.notAuthorized, response);
          break;

        case 419:
        case 440:
          $log.debug('AuthInterceptor - Sessione scaduta (HTTP ' + response.status + ')');
          $rootScope.$broadcast(AuthEvents.sessionTimeout, response);
          break;
      }

      return $q.reject(response);
    }
  }];
}
},{}],2:[function(require,module,exports){

angular
  .module('authentica')
  .provider('AuthService', AuthService);

function AuthService() {
  var config = this;

  this.applicationId = null;
  this.basePath = 'http://localhost:3001';

  this.$get = ['$rootScope', '$http', '$log', '$q', 'Session', 'HttpBuffer', 'AuthEvents', function ($rootScope, $http, $log, $q, Session, HttpBuffer, AuthEvents) {
    var service = {
      login: login,
      logout: logout,
      changePassword: changePassword,
      forgotPassword: forgotPassword,
      signUp: signUp,
      isAuthenticated: isAuthenticated,
      isAuthorized: isAuthorized,
      options: { applicationId: config.applicationId }
    };

    return service;

    function join (base, route) {
      return base + route;
    }

    function login (options) {
      var credentials;

      options = options || {};

      credentials = {
        "username": options.username,
        "password": options.password
      };

      return $http
        .post(join(config.basePath, '/login'), credentials)
        .then(function (res) {
          $log.debug('AuthService - Autenticazione riuscita');
          Session.create(res.data.token);
          $rootScope.$broadcast(AuthEvents.loginSuccess, res.data);
          HttpBuffer.retryAll();
          return res.data;
        });
    }

    function logout () {
      if (!Session.isValid()) {
        return $q.resolve(onLogout());
      }
      
      return $http
        .post(join(config.basePath, '/logout'))
        .then(onLogout);
        
      function onLogout () {
        $log.debug('AuthService - Uscita riuscita');
        Session.destroy();
        $rootScope.$broadcast(AuthEvents.logoutSuccess);
        HttpBuffer.rejectAll();
      }
    }

    function changePassword (options) {
      var credentials;

      options = options || {};

      credentials = {
        "username": options.username,
        "newPassword": options.newPassword,
        "oldPassword": options.oldPassword
      };

      return $http
        .post(join(config.basePath, '/changePassword'), credentials)
        .then(function (res) {
          $log.debug('AuthService - Password cambiata con successo');
          return res.data;
        });
    }

    function forgotPassword (options) {
      var credentials;

      options = options || {};

      credentials = {
        "email": options.email
      };

      return $http
        .post(join(config.basePath, '/forgotPassword'), credentials)
        .then(function (res) {
          return res.data;
        });
    }

    function signUp (options) {
      var credentials;

      options = options || {};

      credentials = {
        "email": options.email,
        "fullname": options.fullname,
        "username": options.username,
        "password": options.password
      };

      return $http
        .post(join(config.basePath, '/signup'), credentials)
        .then(function (res) {
          return res.data;
        });
    }

    function isAuthenticated () {
      return Session.isValid();
    }

    function isAuthorized (roles) {
      var scopes = Session.scopes;

      if (!scopes)
        return false;

      if (!angular.isArray(roles)) {
        roles = [roles];
      }
      return roles.filter(function(role) {
          return scopes.indexOf(role) > -1;
        }).length > 0;
    }
  }];
}
},{}],3:[function(require,module,exports){

angular
  .module('authentica')
  .constant('AuthEvents', {
    loginSuccess: 'auth:login:success',
    loginFailed: 'auth:login:failed',
    logoutSuccess: 'auth:logout:success',
    sessionTimeout: 'auth:session:timeout',
    notAuthenticated: 'auth:not:authenticated',
    notAuthorized: 'auth:not:authorized'
  });
},{}],4:[function(require,module,exports){

angular
  .module('authentica')
  .provider('HttpBuffer', HttpBuffer);

function HttpBuffer() {
  var config = this;

  this.updater = function (config) { return config };

  this.$get = ['$injector', '$log', function ($injector, $log) {
    var service = {}
      , buffer = []
      , $http;

    service.append = append;
    service.retryAll = retryAll;
    service.rejectAll = rejectAll;

    return service;

    function append (config, deferred) {
      buffer.push({ config: config, deferred: deferred });
    }

    function retryAll () {
      for (var i = 0; i < buffer.length; ++i) {
        retry(config.updater(buffer[i].config), buffer[i].deferred);
      }
      buffer = [];
    }

    function rejectAll (reason) {
      if (reason) {
        for (var i = 0; i < buffer.length; ++i) {
          buffer[i].deferred.reject(reason);
        }
      }

      buffer = [];

      $log.debug('HttpBuffer - Buffer pulito.');
    }

    function retry (config, deferred) {
      $http = $http || $injector.get('$http');
      $http(config).then(successFn, errorFn);

      function successFn(response) {
        deferred.resolve(response);
      }
      function errorFn(response) {
        deferred.reject(response);
      }
    }
  }];
}
},{}],5:[function(require,module,exports){

angular
  .module('authentica', ['ngCookies']);
  
require('./jwt.service');
require('./session.service');
require('./http.buffer.service');
require('./events.constant');
require('./auth.service');
require('./auth.interceptor');
},{"./auth.interceptor":1,"./auth.service":2,"./events.constant":3,"./http.buffer.service":4,"./jwt.service":6,"./session.service":7}],6:[function(require,module,exports){

angular
  .module('authentica')
  .factory('JWT', JWTHelper);

JWTHelper.$inject = ['$window'];

function JWTHelper($window) {
  var service = {
    urlBase64Decode: urlBase64Decode,
    decode: decode
  };

  return service;

  function urlBase64Decode (str) {
    var output = str.replace(/-/g, '+').replace(/_/g, '/');
    switch (output.length % 4) {
      case 0: { break; }
      case 2: { output += '=='; break; }
      case 3: { output += '='; break; }
      default: {
        throw 'Illegal base64url string!';
      }
    }
    return $window.decodeURIComponent(escape($window.atob(output))); //polyfill https://github.com/davidchambers/Base64.js
  }

  function decode (token) {
    var parts = token.split('.');

    if (parts.length !== 3) {
      throw new Error('JWT must have 3 parts');
    }

    var decoded = this.urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error('Cannot decode the token');
    }

    return angular.fromJson(decoded);
  }
}
},{}],7:[function(require,module,exports){

angular
  .module('authentica')
  .provider('Session', Session);

function Session () {
  var config = this;
  
  config.storageType = 'cookies';
  
  this.$get = ['$log', '$cookies', 'JWT', function ($log, $cookies, JWT) {
    var service = {};

    service.create = create;
    service.destroy = destroy;
    service.isValid = isValid;

    init();
    
    return service;

    function create (token) {
      var decoded;

      decoded = JWT.decode(token);

      service.token = token;
      service.user = decoded.user;
      service.scopes = decoded.scopes;

      setItem('session', service);

      $log.debug('Sessione creata.');
    }

    function destroy () {
      service.token = null;
      service.user = null;
      service.scopes = [];

      removeItem('session');

      $log.debug('Sessione distrutta.');
    }
    
    function isValid () {
      var clockTimestamp = Math.floor(Date.now() / 1000)
        , valid = !!service.token;
      
      if (typeof service.exp !== 'undefined' && typeof service.exp === 'number')
        valid = !(clockTimestamp >= service.exp) && valid; 
      
      return  valid;
    }

    function init () {
      var stored = getItem('session');
      angular.extend(service, stored);
      $log.debug('Sessione recuperata.');
    }
  
    function removeItem (key) {
      if (storageAvailable('localStorage') && config.storageType == 'localStorage') {
        window.localStorage.removeItem(key);
      }
      else {
        $cookies.remove(key);
      }
    }
  
    function setItem (key, value) {
      if (storageAvailable('localStorage') && config.storageType == 'localStorage') {
        window.localStorage.setItem(key, JSON.stringify(value));
      }
      else {
        $cookies.putObject(key, value);
      }
    }
  
    function getItem (key) {
      var stored;
  
      if (storageAvailable('localStorage') && config.storageType == 'localStorage') {
        stored = window.localStorage.getItem(key);
        stored = JSON.parse(stored);
      }
      else {
        stored = $cookies.getObject(key);
      }
    
      return stored;
    }
  
    function storageAvailable(type) {
    	try {
    		var storage = window[type]
    			, x = '__storage_test__';
  		
        storage.setItem(x, x);
    		storage.removeItem(x);
  		
        return true;
    	}
    	catch(e) {
    		return false;
    	}
    }
  }];
}
},{}]},{},[5])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJsaWIvYXV0aC5pbnRlcmNlcHRvci5qcyIsImxpYi9hdXRoLnNlcnZpY2UuanMiLCJsaWIvZXZlbnRzLmNvbnN0YW50LmpzIiwibGliL2h0dHAuYnVmZmVyLnNlcnZpY2UuanMiLCJsaWIvaW5kZXguanMiLCJsaWIvand0LnNlcnZpY2UuanMiLCJsaWIvc2Vzc2lvbi5zZXJ2aWNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0lBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiXG5hbmd1bGFyXG4gIC5tb2R1bGUoJ2F1dGhlbnRpY2EnKVxuICAucHJvdmlkZXIoJ0F1dGhJbnRlcmNlcHRvcicsIEF1dGhJbnRlcmNlcHRvcik7XG5cbmZ1bmN0aW9uIEF1dGhJbnRlcmNlcHRvcigpIHtcbiAgdmFyIGNvbmZpZyA9IHRoaXM7XG5cbiAgdGhpcy5idWZmZXJlZCA9IHRydWU7XG4gIHRoaXMudXJsUGFyYW0gPSBudWxsO1xuICB0aGlzLmF1dGhIZWFkZXIgPSAnQXV0aG9yaXphdGlvbic7XG4gIHRoaXMuYXV0aFByZWZpeCA9ICdKV1QgJztcblxuICB0aGlzLiRnZXQgPSBbXCIkcm9vdFNjb3BlXCIsIFwiJHFcIiwgXCIkbG9nXCIsIFwiU2Vzc2lvblwiLCBcIkh0dHBCdWZmZXJcIiwgXCJBdXRoRXZlbnRzXCIsIGZ1bmN0aW9uICgkcm9vdFNjb3BlLCAkcSwgJGxvZywgU2Vzc2lvbiwgSHR0cEJ1ZmZlciwgQXV0aEV2ZW50cykge1xuICAgIHJldHVybiB7XG4gICAgICByZXF1ZXN0OiByZXF1ZXN0SGFuZGxlcixcbiAgICAgIHJlc3BvbnNlRXJyb3I6IHJlc3BvbnNlRXJyb3JcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gcmVxdWVzdEhhbmRsZXIgKHJlcXVlc3QpIHtcbiAgICAgIGlmIChyZXF1ZXN0LnNraXBBdXRob3JpemF0aW9uIHx8IHJlcXVlc3QudXJsID09ICcvbG9naW4nKSB7XG4gICAgICAgIHJldHVybiByZXF1ZXN0O1xuICAgICAgfVxuXG4gICAgICAvKmlmIChjb25maWcudXJsUGFyYW0pIHtcbiAgICAgICAgcmVxdWVzdC5wYXJhbXMgPSByZXF1ZXN0LnBhcmFtcyB8fCB7fTtcbiAgICAgICAgaWYgKHJlcXVlc3QucGFyYW1zW2NvbmZpZy51cmxQYXJhbV0pIHtcbiAgICAgICAgICByZXR1cm4gcmVxdWVzdDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmVxdWVzdC5oZWFkZXJzID0gcmVxdWVzdC5oZWFkZXJzIHx8IHt9O1xuICAgICAgICBpZiAocmVxdWVzdC5oZWFkZXJzW2NvbmZpZy5hdXRoSGVhZGVyXSkge1xuICAgICAgICAgIHJldHVybiByZXF1ZXN0O1xuICAgICAgICB9XG4gICAgICB9Ki9cblxuICAgICAgdmFyIHRva2VuID0gU2Vzc2lvbi50b2tlbjtcblxuICAgICAgaWYgKHRva2VuKSB7XG4gICAgICAgIGlmIChjb25maWcudXJsUGFyYW0pIHtcbiAgICAgICAgICByZXF1ZXN0LnBhcmFtc1tjb25maWcudXJsUGFyYW1dID0gdG9rZW47XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVxdWVzdC5oZWFkZXJzW2NvbmZpZy5hdXRoSGVhZGVyXSA9IGNvbmZpZy5hdXRoUHJlZml4ICsgdG9rZW47XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHJlcXVlc3Q7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVzcG9uc2VFcnJvciAocmVzcG9uc2UpIHtcbiAgICAgIHN3aXRjaCAocmVzcG9uc2Uuc3RhdHVzKSB7XG4gICAgICAgIGNhc2UgNDAxOiAvLyB1c2VyIG5vdCBsb2dnZWQgaW5cbiAgICAgICAgICB2YXIgZGVmZXJyZWQ7XG5cbiAgICAgICAgICBpZihyZXNwb25zZS5jb25maWcudXJsLmluZGV4T2YoJy9sb2dpbicpICE9IC0xKVxuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAkbG9nLmRlYnVnKCdBdXRoSW50ZXJjZXB0b3IgLSBVdGVudGUgbm9uIGF1dGVudGljYXRvIChIVFRQIDQwMSknKTtcbiAgICAgICAgICBkZWZlcnJlZCA9ICRxLmRlZmVyKClcblxuICAgICAgICAgIGlmIChjb25maWcuYnVmZmVyZWQpXG4gICAgICAgICAgICBIdHRwQnVmZmVyLmFwcGVuZChyZXNwb25zZS5jb25maWcsIGRlZmVycmVkKTtcblxuICAgICAgICAgIFNlc3Npb24uZGVzdHJveSgpO1xuXG4gICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEF1dGhFdmVudHMubm90QXV0aGVudGljYXRlZCwgcmVzcG9uc2UpO1xuICAgICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgNDAzOlxuICAgICAgICAgICRsb2cuZGVidWcoJ0F1dGhJbnRlcmNlcHRvciAtIFV0ZW50ZSBub24gYXV0b3JpenphdG8gKEhUVFAgNDAzKScpO1xuICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBdXRoRXZlbnRzLm5vdEF1dGhvcml6ZWQsIHJlc3BvbnNlKTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIDQxOTpcbiAgICAgICAgY2FzZSA0NDA6XG4gICAgICAgICAgJGxvZy5kZWJ1ZygnQXV0aEludGVyY2VwdG9yIC0gU2Vzc2lvbmUgc2NhZHV0YSAoSFRUUCAnICsgcmVzcG9uc2Uuc3RhdHVzICsgJyknKTtcbiAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQXV0aEV2ZW50cy5zZXNzaW9uVGltZW91dCwgcmVzcG9uc2UpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gJHEucmVqZWN0KHJlc3BvbnNlKTtcbiAgICB9XG4gIH1dO1xufSIsIlxuYW5ndWxhclxuICAubW9kdWxlKCdhdXRoZW50aWNhJylcbiAgLnByb3ZpZGVyKCdBdXRoU2VydmljZScsIEF1dGhTZXJ2aWNlKTtcblxuZnVuY3Rpb24gQXV0aFNlcnZpY2UoKSB7XG4gIHZhciBjb25maWcgPSB0aGlzO1xuXG4gIHRoaXMuYXBwbGljYXRpb25JZCA9IG51bGw7XG4gIHRoaXMuYmFzZVBhdGggPSAnaHR0cDovL2xvY2FsaG9zdDozMDAxJztcblxuICB0aGlzLiRnZXQgPSBbJyRyb290U2NvcGUnLCAnJGh0dHAnLCAnJGxvZycsICckcScsICdTZXNzaW9uJywgJ0h0dHBCdWZmZXInLCAnQXV0aEV2ZW50cycsIGZ1bmN0aW9uICgkcm9vdFNjb3BlLCAkaHR0cCwgJGxvZywgJHEsIFNlc3Npb24sIEh0dHBCdWZmZXIsIEF1dGhFdmVudHMpIHtcbiAgICB2YXIgc2VydmljZSA9IHtcbiAgICAgIGxvZ2luOiBsb2dpbixcbiAgICAgIGxvZ291dDogbG9nb3V0LFxuICAgICAgY2hhbmdlUGFzc3dvcmQ6IGNoYW5nZVBhc3N3b3JkLFxuICAgICAgZm9yZ290UGFzc3dvcmQ6IGZvcmdvdFBhc3N3b3JkLFxuICAgICAgc2lnblVwOiBzaWduVXAsXG4gICAgICBpc0F1dGhlbnRpY2F0ZWQ6IGlzQXV0aGVudGljYXRlZCxcbiAgICAgIGlzQXV0aG9yaXplZDogaXNBdXRob3JpemVkLFxuICAgICAgb3B0aW9uczogeyBhcHBsaWNhdGlvbklkOiBjb25maWcuYXBwbGljYXRpb25JZCB9XG4gICAgfTtcblxuICAgIHJldHVybiBzZXJ2aWNlO1xuXG4gICAgZnVuY3Rpb24gam9pbiAoYmFzZSwgcm91dGUpIHtcbiAgICAgIHJldHVybiBiYXNlICsgcm91dGU7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbG9naW4gKG9wdGlvbnMpIHtcbiAgICAgIHZhciBjcmVkZW50aWFscztcblxuICAgICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cbiAgICAgIGNyZWRlbnRpYWxzID0ge1xuICAgICAgICBcInVzZXJuYW1lXCI6IG9wdGlvbnMudXNlcm5hbWUsXG4gICAgICAgIFwicGFzc3dvcmRcIjogb3B0aW9ucy5wYXNzd29yZFxuICAgICAgfTtcblxuICAgICAgcmV0dXJuICRodHRwXG4gICAgICAgIC5wb3N0KGpvaW4oY29uZmlnLmJhc2VQYXRoLCAnL2xvZ2luJyksIGNyZWRlbnRpYWxzKVxuICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgICAgJGxvZy5kZWJ1ZygnQXV0aFNlcnZpY2UgLSBBdXRlbnRpY2F6aW9uZSByaXVzY2l0YScpO1xuICAgICAgICAgIFNlc3Npb24uY3JlYXRlKHJlcy5kYXRhLnRva2VuKTtcbiAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQXV0aEV2ZW50cy5sb2dpblN1Y2Nlc3MsIHJlcy5kYXRhKTtcbiAgICAgICAgICBIdHRwQnVmZmVyLnJldHJ5QWxsKCk7XG4gICAgICAgICAgcmV0dXJuIHJlcy5kYXRhO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBsb2dvdXQgKCkge1xuICAgICAgaWYgKCFTZXNzaW9uLmlzVmFsaWQoKSkge1xuICAgICAgICByZXR1cm4gJHEucmVzb2x2ZShvbkxvZ291dCgpKTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgcmV0dXJuICRodHRwXG4gICAgICAgIC5wb3N0KGpvaW4oY29uZmlnLmJhc2VQYXRoLCAnL2xvZ291dCcpKVxuICAgICAgICAudGhlbihvbkxvZ291dCk7XG4gICAgICAgIFxuICAgICAgZnVuY3Rpb24gb25Mb2dvdXQgKCkge1xuICAgICAgICAkbG9nLmRlYnVnKCdBdXRoU2VydmljZSAtIFVzY2l0YSByaXVzY2l0YScpO1xuICAgICAgICBTZXNzaW9uLmRlc3Ryb3koKTtcbiAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEF1dGhFdmVudHMubG9nb3V0U3VjY2Vzcyk7XG4gICAgICAgIEh0dHBCdWZmZXIucmVqZWN0QWxsKCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2hhbmdlUGFzc3dvcmQgKG9wdGlvbnMpIHtcbiAgICAgIHZhciBjcmVkZW50aWFscztcblxuICAgICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cbiAgICAgIGNyZWRlbnRpYWxzID0ge1xuICAgICAgICBcInVzZXJuYW1lXCI6IG9wdGlvbnMudXNlcm5hbWUsXG4gICAgICAgIFwibmV3UGFzc3dvcmRcIjogb3B0aW9ucy5uZXdQYXNzd29yZCxcbiAgICAgICAgXCJvbGRQYXNzd29yZFwiOiBvcHRpb25zLm9sZFBhc3N3b3JkXG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gJGh0dHBcbiAgICAgICAgLnBvc3Qoam9pbihjb25maWcuYmFzZVBhdGgsICcvY2hhbmdlUGFzc3dvcmQnKSwgY3JlZGVudGlhbHMpXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXMpIHtcbiAgICAgICAgICAkbG9nLmRlYnVnKCdBdXRoU2VydmljZSAtIFBhc3N3b3JkIGNhbWJpYXRhIGNvbiBzdWNjZXNzbycpO1xuICAgICAgICAgIHJldHVybiByZXMuZGF0YTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZm9yZ290UGFzc3dvcmQgKG9wdGlvbnMpIHtcbiAgICAgIHZhciBjcmVkZW50aWFscztcblxuICAgICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cbiAgICAgIGNyZWRlbnRpYWxzID0ge1xuICAgICAgICBcImVtYWlsXCI6IG9wdGlvbnMuZW1haWxcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiAkaHR0cFxuICAgICAgICAucG9zdChqb2luKGNvbmZpZy5iYXNlUGF0aCwgJy9mb3Jnb3RQYXNzd29yZCcpLCBjcmVkZW50aWFscylcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHJlcykge1xuICAgICAgICAgIHJldHVybiByZXMuZGF0YTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc2lnblVwIChvcHRpb25zKSB7XG4gICAgICB2YXIgY3JlZGVudGlhbHM7XG5cbiAgICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuXG4gICAgICBjcmVkZW50aWFscyA9IHtcbiAgICAgICAgXCJlbWFpbFwiOiBvcHRpb25zLmVtYWlsLFxuICAgICAgICBcImZ1bGxuYW1lXCI6IG9wdGlvbnMuZnVsbG5hbWUsXG4gICAgICAgIFwidXNlcm5hbWVcIjogb3B0aW9ucy51c2VybmFtZSxcbiAgICAgICAgXCJwYXNzd29yZFwiOiBvcHRpb25zLnBhc3N3b3JkXG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gJGh0dHBcbiAgICAgICAgLnBvc3Qoam9pbihjb25maWcuYmFzZVBhdGgsICcvc2lnbnVwJyksIGNyZWRlbnRpYWxzKVxuICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgICAgcmV0dXJuIHJlcy5kYXRhO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpc0F1dGhlbnRpY2F0ZWQgKCkge1xuICAgICAgcmV0dXJuIFNlc3Npb24uaXNWYWxpZCgpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGlzQXV0aG9yaXplZCAocm9sZXMpIHtcbiAgICAgIHZhciBzY29wZXMgPSBTZXNzaW9uLnNjb3BlcztcblxuICAgICAgaWYgKCFzY29wZXMpXG4gICAgICAgIHJldHVybiBmYWxzZTtcblxuICAgICAgaWYgKCFhbmd1bGFyLmlzQXJyYXkocm9sZXMpKSB7XG4gICAgICAgIHJvbGVzID0gW3JvbGVzXTtcbiAgICAgIH1cbiAgICAgIHJldHVybiByb2xlcy5maWx0ZXIoZnVuY3Rpb24ocm9sZSkge1xuICAgICAgICAgIHJldHVybiBzY29wZXMuaW5kZXhPZihyb2xlKSA+IC0xO1xuICAgICAgICB9KS5sZW5ndGggPiAwO1xuICAgIH1cbiAgfV07XG59IiwiXG5hbmd1bGFyXG4gIC5tb2R1bGUoJ2F1dGhlbnRpY2EnKVxuICAuY29uc3RhbnQoJ0F1dGhFdmVudHMnLCB7XG4gICAgbG9naW5TdWNjZXNzOiAnYXV0aDpsb2dpbjpzdWNjZXNzJyxcbiAgICBsb2dpbkZhaWxlZDogJ2F1dGg6bG9naW46ZmFpbGVkJyxcbiAgICBsb2dvdXRTdWNjZXNzOiAnYXV0aDpsb2dvdXQ6c3VjY2VzcycsXG4gICAgc2Vzc2lvblRpbWVvdXQ6ICdhdXRoOnNlc3Npb246dGltZW91dCcsXG4gICAgbm90QXV0aGVudGljYXRlZDogJ2F1dGg6bm90OmF1dGhlbnRpY2F0ZWQnLFxuICAgIG5vdEF1dGhvcml6ZWQ6ICdhdXRoOm5vdDphdXRob3JpemVkJ1xuICB9KTsiLCJcbmFuZ3VsYXJcbiAgLm1vZHVsZSgnYXV0aGVudGljYScpXG4gIC5wcm92aWRlcignSHR0cEJ1ZmZlcicsIEh0dHBCdWZmZXIpO1xuXG5mdW5jdGlvbiBIdHRwQnVmZmVyKCkge1xuICB2YXIgY29uZmlnID0gdGhpcztcblxuICB0aGlzLnVwZGF0ZXIgPSBmdW5jdGlvbiAoY29uZmlnKSB7IHJldHVybiBjb25maWcgfTtcblxuICB0aGlzLiRnZXQgPSBbJyRpbmplY3RvcicsICckbG9nJywgZnVuY3Rpb24gKCRpbmplY3RvciwgJGxvZykge1xuICAgIHZhciBzZXJ2aWNlID0ge31cbiAgICAgICwgYnVmZmVyID0gW11cbiAgICAgICwgJGh0dHA7XG5cbiAgICBzZXJ2aWNlLmFwcGVuZCA9IGFwcGVuZDtcbiAgICBzZXJ2aWNlLnJldHJ5QWxsID0gcmV0cnlBbGw7XG4gICAgc2VydmljZS5yZWplY3RBbGwgPSByZWplY3RBbGw7XG5cbiAgICByZXR1cm4gc2VydmljZTtcblxuICAgIGZ1bmN0aW9uIGFwcGVuZCAoY29uZmlnLCBkZWZlcnJlZCkge1xuICAgICAgYnVmZmVyLnB1c2goeyBjb25maWc6IGNvbmZpZywgZGVmZXJyZWQ6IGRlZmVycmVkIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJldHJ5QWxsICgpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYnVmZmVyLmxlbmd0aDsgKytpKSB7XG4gICAgICAgIHJldHJ5KGNvbmZpZy51cGRhdGVyKGJ1ZmZlcltpXS5jb25maWcpLCBidWZmZXJbaV0uZGVmZXJyZWQpO1xuICAgICAgfVxuICAgICAgYnVmZmVyID0gW107XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVqZWN0QWxsIChyZWFzb24pIHtcbiAgICAgIGlmIChyZWFzb24pIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBidWZmZXIubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICBidWZmZXJbaV0uZGVmZXJyZWQucmVqZWN0KHJlYXNvbik7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgYnVmZmVyID0gW107XG5cbiAgICAgICRsb2cuZGVidWcoJ0h0dHBCdWZmZXIgLSBCdWZmZXIgcHVsaXRvLicpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJldHJ5IChjb25maWcsIGRlZmVycmVkKSB7XG4gICAgICAkaHR0cCA9ICRodHRwIHx8ICRpbmplY3Rvci5nZXQoJyRodHRwJyk7XG4gICAgICAkaHR0cChjb25maWcpLnRoZW4oc3VjY2Vzc0ZuLCBlcnJvckZuKTtcblxuICAgICAgZnVuY3Rpb24gc3VjY2Vzc0ZuKHJlc3BvbnNlKSB7XG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgfVxuICAgICAgZnVuY3Rpb24gZXJyb3JGbihyZXNwb25zZSkge1xuICAgICAgICBkZWZlcnJlZC5yZWplY3QocmVzcG9uc2UpO1xuICAgICAgfVxuICAgIH1cbiAgfV07XG59IiwiXG5hbmd1bGFyXG4gIC5tb2R1bGUoJ2F1dGhlbnRpY2EnLCBbJ25nQ29va2llcyddKTtcbiAgXG5yZXF1aXJlKCcuL2p3dC5zZXJ2aWNlJyk7XG5yZXF1aXJlKCcuL3Nlc3Npb24uc2VydmljZScpO1xucmVxdWlyZSgnLi9odHRwLmJ1ZmZlci5zZXJ2aWNlJyk7XG5yZXF1aXJlKCcuL2V2ZW50cy5jb25zdGFudCcpO1xucmVxdWlyZSgnLi9hdXRoLnNlcnZpY2UnKTtcbnJlcXVpcmUoJy4vYXV0aC5pbnRlcmNlcHRvcicpOyIsIlxuYW5ndWxhclxuICAubW9kdWxlKCdhdXRoZW50aWNhJylcbiAgLmZhY3RvcnkoJ0pXVCcsIEpXVEhlbHBlcik7XG5cbkpXVEhlbHBlci4kaW5qZWN0ID0gWyckd2luZG93J107XG5cbmZ1bmN0aW9uIEpXVEhlbHBlcigkd2luZG93KSB7XG4gIHZhciBzZXJ2aWNlID0ge1xuICAgIHVybEJhc2U2NERlY29kZTogdXJsQmFzZTY0RGVjb2RlLFxuICAgIGRlY29kZTogZGVjb2RlXG4gIH07XG5cbiAgcmV0dXJuIHNlcnZpY2U7XG5cbiAgZnVuY3Rpb24gdXJsQmFzZTY0RGVjb2RlIChzdHIpIHtcbiAgICB2YXIgb3V0cHV0ID0gc3RyLnJlcGxhY2UoLy0vZywgJysnKS5yZXBsYWNlKC9fL2csICcvJyk7XG4gICAgc3dpdGNoIChvdXRwdXQubGVuZ3RoICUgNCkge1xuICAgICAgY2FzZSAwOiB7IGJyZWFrOyB9XG4gICAgICBjYXNlIDI6IHsgb3V0cHV0ICs9ICc9PSc7IGJyZWFrOyB9XG4gICAgICBjYXNlIDM6IHsgb3V0cHV0ICs9ICc9JzsgYnJlYWs7IH1cbiAgICAgIGRlZmF1bHQ6IHtcbiAgICAgICAgdGhyb3cgJ0lsbGVnYWwgYmFzZTY0dXJsIHN0cmluZyEnO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gJHdpbmRvdy5kZWNvZGVVUklDb21wb25lbnQoZXNjYXBlKCR3aW5kb3cuYXRvYihvdXRwdXQpKSk7IC8vcG9seWZpbGwgaHR0cHM6Ly9naXRodWIuY29tL2RhdmlkY2hhbWJlcnMvQmFzZTY0LmpzXG4gIH1cblxuICBmdW5jdGlvbiBkZWNvZGUgKHRva2VuKSB7XG4gICAgdmFyIHBhcnRzID0gdG9rZW4uc3BsaXQoJy4nKTtcblxuICAgIGlmIChwYXJ0cy5sZW5ndGggIT09IDMpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignSldUIG11c3QgaGF2ZSAzIHBhcnRzJyk7XG4gICAgfVxuXG4gICAgdmFyIGRlY29kZWQgPSB0aGlzLnVybEJhc2U2NERlY29kZShwYXJ0c1sxXSk7XG4gICAgaWYgKCFkZWNvZGVkKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Nhbm5vdCBkZWNvZGUgdGhlIHRva2VuJyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGFuZ3VsYXIuZnJvbUpzb24oZGVjb2RlZCk7XG4gIH1cbn0iLCJcbmFuZ3VsYXJcbiAgLm1vZHVsZSgnYXV0aGVudGljYScpXG4gIC5wcm92aWRlcignU2Vzc2lvbicsIFNlc3Npb24pO1xuXG5mdW5jdGlvbiBTZXNzaW9uICgpIHtcbiAgdmFyIGNvbmZpZyA9IHRoaXM7XG4gIFxuICBjb25maWcuc3RvcmFnZVR5cGUgPSAnY29va2llcyc7XG4gIFxuICB0aGlzLiRnZXQgPSBbJyRsb2cnLCAnJGNvb2tpZXMnLCAnSldUJywgZnVuY3Rpb24gKCRsb2csICRjb29raWVzLCBKV1QpIHtcbiAgICB2YXIgc2VydmljZSA9IHt9O1xuXG4gICAgc2VydmljZS5jcmVhdGUgPSBjcmVhdGU7XG4gICAgc2VydmljZS5kZXN0cm95ID0gZGVzdHJveTtcbiAgICBzZXJ2aWNlLmlzVmFsaWQgPSBpc1ZhbGlkO1xuXG4gICAgaW5pdCgpO1xuICAgIFxuICAgIHJldHVybiBzZXJ2aWNlO1xuXG4gICAgZnVuY3Rpb24gY3JlYXRlICh0b2tlbikge1xuICAgICAgdmFyIGRlY29kZWQ7XG5cbiAgICAgIGRlY29kZWQgPSBKV1QuZGVjb2RlKHRva2VuKTtcblxuICAgICAgc2VydmljZS50b2tlbiA9IHRva2VuO1xuICAgICAgc2VydmljZS51c2VyID0gZGVjb2RlZC51c2VyO1xuICAgICAgc2VydmljZS5zY29wZXMgPSBkZWNvZGVkLnNjb3BlcztcblxuICAgICAgc2V0SXRlbSgnc2Vzc2lvbicsIHNlcnZpY2UpO1xuXG4gICAgICAkbG9nLmRlYnVnKCdTZXNzaW9uZSBjcmVhdGEuJyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZGVzdHJveSAoKSB7XG4gICAgICBzZXJ2aWNlLnRva2VuID0gbnVsbDtcbiAgICAgIHNlcnZpY2UudXNlciA9IG51bGw7XG4gICAgICBzZXJ2aWNlLnNjb3BlcyA9IFtdO1xuXG4gICAgICByZW1vdmVJdGVtKCdzZXNzaW9uJyk7XG5cbiAgICAgICRsb2cuZGVidWcoJ1Nlc3Npb25lIGRpc3RydXR0YS4nKTtcbiAgICB9XG4gICAgXG4gICAgZnVuY3Rpb24gaXNWYWxpZCAoKSB7XG4gICAgICB2YXIgY2xvY2tUaW1lc3RhbXAgPSBNYXRoLmZsb29yKERhdGUubm93KCkgLyAxMDAwKVxuICAgICAgICAsIHZhbGlkID0gISFzZXJ2aWNlLnRva2VuO1xuICAgICAgXG4gICAgICBpZiAodHlwZW9mIHNlcnZpY2UuZXhwICE9PSAndW5kZWZpbmVkJyAmJiB0eXBlb2Ygc2VydmljZS5leHAgPT09ICdudW1iZXInKVxuICAgICAgICB2YWxpZCA9ICEoY2xvY2tUaW1lc3RhbXAgPj0gc2VydmljZS5leHApICYmIHZhbGlkOyBcbiAgICAgIFxuICAgICAgcmV0dXJuICB2YWxpZDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbml0ICgpIHtcbiAgICAgIHZhciBzdG9yZWQgPSBnZXRJdGVtKCdzZXNzaW9uJyk7XG4gICAgICBhbmd1bGFyLmV4dGVuZChzZXJ2aWNlLCBzdG9yZWQpO1xuICAgICAgJGxvZy5kZWJ1ZygnU2Vzc2lvbmUgcmVjdXBlcmF0YS4nKTtcbiAgICB9XG4gIFxuICAgIGZ1bmN0aW9uIHJlbW92ZUl0ZW0gKGtleSkge1xuICAgICAgaWYgKHN0b3JhZ2VBdmFpbGFibGUoJ2xvY2FsU3RvcmFnZScpICYmIGNvbmZpZy5zdG9yYWdlVHlwZSA9PSAnbG9jYWxTdG9yYWdlJykge1xuICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oa2V5KTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAkY29va2llcy5yZW1vdmUoa2V5KTtcbiAgICAgIH1cbiAgICB9XG4gIFxuICAgIGZ1bmN0aW9uIHNldEl0ZW0gKGtleSwgdmFsdWUpIHtcbiAgICAgIGlmIChzdG9yYWdlQXZhaWxhYmxlKCdsb2NhbFN0b3JhZ2UnKSAmJiBjb25maWcuc3RvcmFnZVR5cGUgPT0gJ2xvY2FsU3RvcmFnZScpIHtcbiAgICAgICAgd2luZG93LmxvY2FsU3RvcmFnZS5zZXRJdGVtKGtleSwgSlNPTi5zdHJpbmdpZnkodmFsdWUpKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAkY29va2llcy5wdXRPYmplY3Qoa2V5LCB2YWx1ZSk7XG4gICAgICB9XG4gICAgfVxuICBcbiAgICBmdW5jdGlvbiBnZXRJdGVtIChrZXkpIHtcbiAgICAgIHZhciBzdG9yZWQ7XG4gIFxuICAgICAgaWYgKHN0b3JhZ2VBdmFpbGFibGUoJ2xvY2FsU3RvcmFnZScpICYmIGNvbmZpZy5zdG9yYWdlVHlwZSA9PSAnbG9jYWxTdG9yYWdlJykge1xuICAgICAgICBzdG9yZWQgPSB3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0oa2V5KTtcbiAgICAgICAgc3RvcmVkID0gSlNPTi5wYXJzZShzdG9yZWQpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHN0b3JlZCA9ICRjb29raWVzLmdldE9iamVjdChrZXkpO1xuICAgICAgfVxuICAgIFxuICAgICAgcmV0dXJuIHN0b3JlZDtcbiAgICB9XG4gIFxuICAgIGZ1bmN0aW9uIHN0b3JhZ2VBdmFpbGFibGUodHlwZSkge1xuICAgIFx0dHJ5IHtcbiAgICBcdFx0dmFyIHN0b3JhZ2UgPSB3aW5kb3dbdHlwZV1cbiAgICBcdFx0XHQsIHggPSAnX19zdG9yYWdlX3Rlc3RfXyc7XG4gIFx0XHRcbiAgICAgICAgc3RvcmFnZS5zZXRJdGVtKHgsIHgpO1xuICAgIFx0XHRzdG9yYWdlLnJlbW92ZUl0ZW0oeCk7XG4gIFx0XHRcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgXHR9XG4gICAgXHRjYXRjaChlKSB7XG4gICAgXHRcdHJldHVybiBmYWxzZTtcbiAgICBcdH1cbiAgICB9XG4gIH1dO1xufSJdfQ==
