
angular
  .module('authentica', ['ngCookies']);
  
require('./jwt.service');
require('./session.service');
require('./http.buffer.service');
require('./events.constant');
require('./auth.service');
require('./auth.interceptor');