
angular
  .module('authentica')
  .provider('Session', Session);

function Session () {
  var config = this;
  
  config.storageType = 'cookies';
  
  this.$get = ['$log', '$cookies', 'JWT', function ($log, $cookies, JWT) {
    var service = {};

    service.create = create;
    service.destroy = destroy;
    service.isValid = isValid;

    init();
    
    return service;

    function create (token) {
      var decoded;

      decoded = JWT.decode(token);

      service.token = token;
      service.user = decoded.user;
      service.scopes = decoded.scopes;

      setItem('session', service);

      $log.debug('Sessione creata.');
    }

    function destroy () {
      service.token = null;
      service.user = null;
      service.scopes = [];

      removeItem('session');

      $log.debug('Sessione distrutta.');
    }
    
    function isValid () {
      var clockTimestamp = Math.floor(Date.now() / 1000)
        , valid = !!service.token;
      
      if (typeof service.exp !== 'undefined' && typeof service.exp === 'number')
        valid = !(clockTimestamp >= service.exp) && valid; 
      
      return  valid;
    }

    function init () {
      var stored = getItem('session');
      angular.extend(service, stored);
      $log.debug('Sessione recuperata.');
    }
  
    function removeItem (key) {
      if (storageAvailable('localStorage') && config.storageType == 'localStorage') {
        window.localStorage.removeItem(key);
      }
      else {
        $cookies.remove(key);
      }
    }
  
    function setItem (key, value) {
      if (storageAvailable('localStorage') && config.storageType == 'localStorage') {
        window.localStorage.setItem(key, JSON.stringify(value));
      }
      else {
        $cookies.putObject(key, value);
      }
    }
  
    function getItem (key) {
      var stored;
  
      if (storageAvailable('localStorage') && config.storageType == 'localStorage') {
        stored = window.localStorage.getItem(key);
        stored = JSON.parse(stored);
      }
      else {
        stored = $cookies.getObject(key);
      }
    
      return stored;
    }
  
    function storageAvailable(type) {
    	try {
    		var storage = window[type]
    			, x = '__storage_test__';
  		
        storage.setItem(x, x);
    		storage.removeItem(x);
  		
        return true;
    	}
    	catch(e) {
    		return false;
    	}
    }
  }];
}