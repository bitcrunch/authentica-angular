
angular
  .module('authentica')
  .provider('HttpBuffer', HttpBuffer);

function HttpBuffer() {
  var config = this;

  this.updater = function (config) { return config };

  this.$get = ['$injector', '$log', function ($injector, $log) {
    var service = {}
      , buffer = []
      , $http;

    service.append = append;
    service.retryAll = retryAll;
    service.rejectAll = rejectAll;

    return service;

    function append (config, deferred) {
      buffer.push({ config: config, deferred: deferred });
    }

    function retryAll () {
      for (var i = 0; i < buffer.length; ++i) {
        retry(config.updater(buffer[i].config), buffer[i].deferred);
      }
      buffer = [];
    }

    function rejectAll (reason) {
      if (reason) {
        for (var i = 0; i < buffer.length; ++i) {
          buffer[i].deferred.reject(reason);
        }
      }

      buffer = [];

      $log.debug('HttpBuffer - Buffer pulito.');
    }

    function retry (config, deferred) {
      $http = $http || $injector.get('$http');
      $http(config).then(successFn, errorFn);

      function successFn(response) {
        deferred.resolve(response);
      }
      function errorFn(response) {
        deferred.reject(response);
      }
    }
  }];
}