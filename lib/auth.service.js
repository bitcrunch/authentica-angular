
angular
  .module('authentica')
  .provider('AuthService', AuthService);

function AuthService() {
  var config = this;

  this.applicationId = null;
  this.basePath = 'http://localhost:3001';

  this.$get = ['$rootScope', '$http', '$log', '$q', 'Session', 'HttpBuffer', 'AuthEvents', function ($rootScope, $http, $log, $q, Session, HttpBuffer, AuthEvents) {
    var service = {
      login: login,
      logout: logout,
      changePassword: changePassword,
      forgotPassword: forgotPassword,
      signUp: signUp,
      isAuthenticated: isAuthenticated,
      isAuthorized: isAuthorized,
      options: { applicationId: config.applicationId }
    };

    return service;

    function join (base, route) {
      return base + route;
    }

    function login (options) {
      var credentials;

      options = options || {};

      credentials = {
        "username": options.username,
        "password": options.password
      };

      return $http
        .post(join(config.basePath, '/login'), credentials)
        .then(function (res) {
          $log.debug('AuthService - Autenticazione riuscita');
          Session.create(res.data.token);
          $rootScope.$broadcast(AuthEvents.loginSuccess, res.data);
          HttpBuffer.retryAll();
          return res.data;
        });
    }

    function logout () {
      if (!Session.isValid()) {
        return $q.resolve(onLogout());
      }
      
      return $http
        .post(join(config.basePath, '/logout'))
        .then(onLogout);
        
      function onLogout () {
        $log.debug('AuthService - Uscita riuscita');
        Session.destroy();
        $rootScope.$broadcast(AuthEvents.logoutSuccess);
        HttpBuffer.rejectAll();
      }
    }

    function changePassword (options) {
      var credentials;

      options = options || {};

      credentials = {
        "username": options.username,
        "newPassword": options.newPassword,
        "oldPassword": options.oldPassword
      };

      return $http
        .post(join(config.basePath, '/changePassword'), credentials)
        .then(function (res) {
          $log.debug('AuthService - Password cambiata con successo');
          return res.data;
        });
    }

    function forgotPassword (options) {
      var credentials;

      options = options || {};

      credentials = {
        "email": options.email
      };

      return $http
        .post(join(config.basePath, '/forgotPassword'), credentials)
        .then(function (res) {
          return res.data;
        });
    }

    function signUp (options) {
      var credentials;

      options = options || {};

      credentials = {
        "email": options.email,
        "fullname": options.fullname,
        "username": options.username,
        "password": options.password
      };

      return $http
        .post(join(config.basePath, '/signup'), credentials)
        .then(function (res) {
          return res.data;
        });
    }

    function isAuthenticated () {
      return Session.isValid();
    }

    function isAuthorized (roles) {
      var scopes = Session.scopes;

      if (!scopes)
        return false;

      if (!angular.isArray(roles)) {
        roles = [roles];
      }
      return roles.filter(function(role) {
          return scopes.indexOf(role) > -1;
        }).length > 0;
    }
  }];
}