
angular
  .module('authentica')
  .provider('AuthInterceptor', AuthInterceptor);

function AuthInterceptor() {
  var config = this;

  this.buffered = true;
  this.urlParam = null;
  this.authHeader = 'Authorization';
  this.authPrefix = 'JWT ';

  this.$get = ["$rootScope", "$q", "$log", "Session", "HttpBuffer", "AuthEvents", function ($rootScope, $q, $log, Session, HttpBuffer, AuthEvents) {
    return {
      request: requestHandler,
      responseError: responseError
    };

    function requestHandler (request) {
      if (request.skipAuthorization || request.url == '/login') {
        return request;
      }

      /*if (config.urlParam) {
        request.params = request.params || {};
        if (request.params[config.urlParam]) {
          return request;
        }
      } else {
        request.headers = request.headers || {};
        if (request.headers[config.authHeader]) {
          return request;
        }
      }*/

      var token = Session.token;

      if (token) {
        if (config.urlParam) {
          request.params[config.urlParam] = token;
        } else {
          request.headers[config.authHeader] = config.authPrefix + token;
        }
      }

      return request;
    }

    function responseError (response) {
      switch (response.status) {
        case 401: // user not logged in
          var deferred;

          if(response.config.url.indexOf('/login') != -1)
            break;

          $log.debug('AuthInterceptor - Utente non autenticato (HTTP 401)');
          deferred = $q.defer()

          if (config.buffered)
            HttpBuffer.append(response.config, deferred);

          Session.destroy();

          $rootScope.$broadcast(AuthEvents.notAuthenticated, response);
          return deferred.promise;
          break;

        case 403:
          $log.debug('AuthInterceptor - Utente non autorizzato (HTTP 403)');
          $rootScope.$broadcast(AuthEvents.notAuthorized, response);
          break;

        case 419:
        case 440:
          $log.debug('AuthInterceptor - Sessione scaduta (HTTP ' + response.status + ')');
          $rootScope.$broadcast(AuthEvents.sessionTimeout, response);
          break;
      }

      return $q.reject(response);
    }
  }];
}